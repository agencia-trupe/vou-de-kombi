<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projeto extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
   		$this->load->view('projeto/index');
    }

    function alice(){
    	$this->load->view('projeto/alice');	
    }

    function inesefranco(){
    	$this->load->view('projeto/inesefranco');
    }
}