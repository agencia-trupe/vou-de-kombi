<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
   		$this->load->model('chamadas_model', 'chamadas');
   		$this->load->model('frases_model', 'frases');
        $this->load->model('banners_model', 'banners');
    }

    function index(){
        $data['banners'] = $this->banners->pegarTodos("RAND()");
    	$data['chamadas'] = $this->chamadas->pegarTodos();
    	$data['frases_azuis'] = $this->frases->pegarPorCampo('caixa', 1, "RAND()");
    	$data['frases_pretas'] = $this->frases->pegarPorCampo('caixa', 2, "RAND()");
   		$this->load->view('home', $data);
    }
}