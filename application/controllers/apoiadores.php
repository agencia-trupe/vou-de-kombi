<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apoiadores extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$this->load->model('apoiadores_model', 'model');

    	$data['apoio'] = $this->model->pegarTodos('ordem', 'ASC');
   		
   		$this->load->view('apoiadores', $data);
    }

}