<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galeria extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$this->load->model('galeria_model', 'model');

    	$this->headervar['load_css'] = 'home';
        
    	$data['imagens'] = $this->model->pegarTodos('ordem', 'ASC');
   		
   		$this->load->view('galeria', $data);
    }

}