<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roteiro extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Roteiro";
		$this->unidade = "Destino";
		$this->load->model('roteiro_model', 'model');
	}

	// PRÓXIMOS DESTINOS chegamos = 0
    function index($pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("painel/".$this->router->class."/index/"),
            'per_page' => 20,
            'uri_segment' => 4,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroResultados(0)
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->pegarPaginado(0,$pag_options['per_page'], $pag);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo.' - Próximos Destinos';
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    // JÁ PASSAMOS chegamos = 1
    function passamos($pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("painel/".$this->router->class."/passamos/"),
            'per_page' => 20,
            'uri_segment' => 4,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroResultados(1)
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->pegarPaginado(1,$pag_options['per_page'], $pag);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo.' - Já Passamos';
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['registro'] = FALSE;
        }

        $data['titulo'] = $this->titulo.' - Editar Destino';
        $data['unidade'] = $this->unidade;
        $data['paises'] = $this->db->order_by('nome')->get('paises')->result();
        $data['diario'] = $this->db->order_by('id_blog_categorias')->get('blog')->result();
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }

    function alterar($id){
        if($this->model->alterar($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/passamos', 'refresh');
    }

    function googlemaps(){
        redirect('painel/roteiro');
        $this->load->view('painel/roteiro/kml');
    }
}