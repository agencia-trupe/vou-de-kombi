<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chamadas extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Chamadas";
		$this->unidade = "Chamada";
		$this->load->model('chamadas_model', 'model');
	}

    function index($pag = 0){

        $data['registros'] = $this->model->pegarTodos('ordem', 'ASC');

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }
}