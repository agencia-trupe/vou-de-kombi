<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frases extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Frases";
		$this->unidade = "Frase";
		$this->load->model('frases_model', 'model');
	}

}