<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diario extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
   		$this->load->model('blog_model', 'diario');
    }

    function index($pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("diario/index/"),
            'per_page' => 50,
            'uri_segment' => 3,
            'next_link' => "Próxima →",
            'next_tag_open' => "<div class='next'>",
            'next_tag_close' => '</div>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<div class='prev'>",
            'prev_tag_close' => '</div>',
            'display_pages' => FALSE,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'total_rows' => $this->diario->numeroResultados()
        );
        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

    	$data['posts'] = $this->diario->pegarPaginado($pag_options['per_page'], $pag, $order_campo = 'data', $order = 'DESC');
    	$data['categoria'] = false;
    	foreach ($data['posts'] as $key => $value) {
    		$imagens = $this->diario->imagens($value->id);
    		$value->thumb = isset($imagens[0]) ? $imagens[0] : false;
    		$value->num_comentarios = $this->diario->num_comentarios($value->id);
    	}

   		$this->load->view('diario', $data);
    }

    function ler($slug = false){
        if (!$slug)
            redirect('diario/index');
        
        $data['query'] = $this->diario->pegarPorSlug($slug);

        if (!$data['query'])
            redirect('diario/index');

        $data['imagens'] = $this->diario->imagens($data['query']->id);

        $data['paginacao'] = $this->diario->paginacaoPosts($data['query']->id, $data['query']->data);

        $data['comentarios'] = $this->diario->comentarios($data['query']->id);

        $data['categorias_recentes'] = $this->diario->categoriasRecentes();

        $data['post_recente'] = $this->diario->postRecente($data['query']->id);

        $data['tres_anteriores'] = $this->diario->tresPostsAnteriores($data['query']->data);

        $this->load->view('diario-detalhes', $data);   
    }

    function comentar(){
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $post = $this->input->post('post');
        $mensagem = strip_tags($this->input->post('mensagem'));
        $check_input = $this->input->post('email_');

        $check_str = true;
        $palavrasSpam = array(
            'viagra', 'levitra', 'cialis', 'provigil','buy',
            'semenax','kamagra','hgh','dapoxetine','valium',
            'tadalafil','klonopin','prescription','vigrx',
            'propecia','xenical','metformin','casino','celebrity',
            'pokies','difuclan','oxycodone','penis','prozac',
            'cheap','free','pills','poker','drugs',
            'bacoflen','antabuse','diflucan','valtrex','acomplia',
            'propranolol','singulair','pharmacy','ambien','sildenafil',
            'propecia','cholesterol','valtrex','drug','singulair',
            'priligy','tramadol','overdose','sperm','plavix',
            'electronic cigarette', 'xanax', 'ativan', 'fioricet', 'ampicillin',
            'finasteride', 'actos', 'bactrim', 'accutane', 'celebrex',
            'erythromycin', 'celexa', 'cymbalta', 'doxycycline', 'prednisone',
            'clomid', 'modafinil', 'ativan', 'vicodin', 'prednisolone',
            'alopeci', 'bactrim'
        );
        
        foreach ($palavrasSpam as $key => $value) {
            if(stripos($nome, $value) !== FALSE || stripos($email, $value) !== FALSE || stripos($mensagem, $value) !== FALSE){
                $check_str = false;
            }
        }

        $check_consoantes = '/([bcdfghjklmnpqrstvwxyz]{3})/i';
        if(preg_match($check_consoantes, $nome) == 1 || preg_match($check_consoantes, $email) == 1 || preg_match($check_consoantes, $mensagem) == 1){
            $check_str = false;
        }

        if($check_input == '' && $nome != '' && $mensagem != '' && $check_str){
            $this->db->set('nome', $nome)
                     ->set('email', $email)
                     ->set('id_blog', $post)
                     ->set('comentario', $mensagem)
                     ->set('data', date('Y-m-d'))
                     ->set('datetime', date('Y-m-d H:i:s'))
                     ->insert('blog_comentarios');
            $query = $this->db->get_where('blog', array('id' => $post))->result();
            if(isset($query[0]))
                redirect('diario/ler/'.$query[0]->slug);
            else
                redirect('diario');
        }else{
            redirect('diario');
        }
    }

    function categoria($slug = false){
        $categoria = $this->db->get_where('blog_categorias', array('slug' => $slug))->result();

        if (!isset($categoria[0])) {
            redirect('diario');
        }

        $data['posts'] = $this->diario->pegarPorCampo('id_blog_categorias', $categoria[0]->id);
        $data['categoria'] = $categoria;
        foreach ($data['posts'] as $key => $value) {
            $imagens = $this->diario->imagens($value->id);
            $value->thumb = isset($imagens[0]) ? $imagens[0] : false;
            $value->num_comentarios = $this->diario->num_comentarios($value->id);
        }

        $this->load->view('diario', $data);
    }

}