<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roteiro extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
   		$this->load->model('roteiro_model', 'roteiro');
    }

    function index(){
    	$data['passamos'] = $this->roteiro->pegarTodos(1);
    	$data['proximos'] = $this->roteiro->pegarTodos(0);
    	$data['atual'] = array_shift($data['passamos']);
    	$data['lista_paises'] = $this->db->query("SELECT DISTINCT pais FROM roteiro WHERE data_chegada IS NOT NULL")->result();
   		$this->load->view('roteiro', $data);
    }

    function album($id = false){
        if (!$id){
            $qry = $this->db->query('SELECT * FROM roteiro WHERE data_chegada IS NOT NULL ORDER BY data_chegada DESC LIMIT 1')->result();
            $id = $qry[0]->id;
        }
        
        $data['detalhe'] = $this->roteiro->pegarPorId($id);
        $data['anterior'] = $this->roteiro->anterior($id);
        $data['proximo'] = $this->roteiro->proximo($id);
        $data['lista_paises'] = $this->db->query("SELECT DISTINCT pais FROM roteiro WHERE data_chegada IS NOT NULL")->result();
        foreach ($data['lista_paises'] as $key => $value) {
            $value->link = $this->roteiro->maisRecentePorPais($value->pais);
        }

        $this->load->view('roteiro-detalhes', $data);   
    }
}