<div class="corpo">
	
	<h1 class="fit">O PROJETO</h1>

	<div class="f">
		<p class="padrao">
			Vou de Kombi é um projeto de cunho antropológico, cultural, social, artístico, ambiental e ecológico. Nele, o casal de fotógrafos documentaristas percorrerão mais de 200.000km em uma Kombi adaptada em casa-escritório por países dos 4 continentes (Americano, Europeu, Asiático e Africano) em aproximadamente 5 anos.
		</p>
		<p class="padrao">
			A viagem tem como objetivo o intercâmbio cultural e o registro etnográfico (conhecer por observação direta, inserindo-se na vida cotidiana de um grupo para compreender as relações sócio-culturais, os comportamentos, ritos, técnicas, saberes e práticas) das sociedades visitadas.
		</p>
		<p class="padrao">
			Complementando este, outro objetivo é pesquisar, conhecer e divulgar exemplos de outras culturas que criaram modelos ecológicos de compartilhamento do espaço ambiental, mantendo o equilíbrio entre desenvolvimento e preservação do meio ambiente.
		</p>
		<p class="padrao">
			Vou de Kombi tem o selo “carbon free”. O projeto teve a emissão de gases de efeito estufa inventariada utilizando a metodologia GHG Protocol. Para compensar a emissão de carbono nos primeiros 25.000 quilômetros da viagem, trecho Brasil-Panamá, serão plantadas 32 árvores (restauração da mata atlântica).
		</p>
		<p class="padrao">
			O diário dos viajantes e os resultados da pesquisa serão divulgados neste site em vídeos, textos, fotos e podcast. Também poderão ser acompanhadas pelo twitter e facebook.
		</p>
		<p class="padrao">
			Em viagem lançaremos promoções em nosso site e redes sociais. Fique ligado em nossa página!
		</p>
	</div>

	<aside>

		<a href="projeto/alice" title="Conheça a Kombi Alice" class="link-alice">
			<h2 class="fit">CONHEÇA A KOMBI ALICE</h2>
			<img src="_imgs/layout/alice-gde.png" alt="Kombi Alice">
			<div class="circle1">
				VOCÊ SABE TUDO O QUE A ALICE TEM DE ESPECIAL?
			</div>
			<div class="circle2">CONFIRA!</div>
		</a>
		
		<a href="projeto/ines-e-franco" title="Conheça a Inês e o Franco" class="link-inesfranco">
			<h2 class="fit">Conheça a Inês e o Franco</h2>
			<img src="_imgs/layout/inesfranco-peq.png" alt="Inês e Franco">
		</a>

	</aside>

</div>