<div class="corpo">
	
	<h1 class="fit"><a href='projeto' title='voltar para o projeto'>O PROJETO<img src='_imgs/layout/seta-voltar.png' alt="seta de voltar"></a></h1>

	<div class="f">
		<h2>CONHEÇA A KOMBI ALICE</h2>
		 
		<p class="padrao">
			Ela é nossa Kombi e companheira de estrada. Na primeira edição do projeto, nós e a Alice percorremos 60.000 km pelo Brasil, visitando 400 cidades e levando trabalhos sociais para mais de 500 crianças. Agora, no roteiro internacional, a Alice ganhou uma nova versão, mais sustentável e com a cara do projeto.
		</p>
		<p class="padrao"> 
			Com a ajuda de nossos colaboradores transformamos a Alice em uma kombi simpática e amiga do meio ambiente. Alice possui:
		</p>
		<p class="padrao">
		- Energia limpa gerada pelo teto solar da kombi.<br>
		- Selo Carbon free (leia a respeito do selo apoiado pela Iniciatiava Verde) <a href='http://www.iniciativaverde.org.br/' title="Iniciativa Verde" target="_blank">http://www.iniciativaverde.org.br/</a><br>
		- Sistema de iluminação por LED com ultra baixo consumo.<br>
		- Passagens por áreas verdes e preservadas
		</p>
	</div>

	<aside>
		<img src="_imgs/layout/alice-gde.png" alt="Kombi Alice">		
		<img src="_imgs/layout/bolas-dois.png" alt="Kombi Alice" style="margin-top:-30px">		
	</aside>

</div>