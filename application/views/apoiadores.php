<div class="corpo">
	
	<h1 class="fit">APOIADORES</h1>
	<h2>Veja quem já é apoiador dessa ideia!</h2>

	<ul class="lista-apoiadores">
		<?php if($apoio): ?>
			<?php foreach ($apoio as $key => $value): ?>
				<li>
					<a href="<?=prep_url($value->link)?>" title="<?=$value->titulo?>" target="_blank">
						<img src="_imgs/apoio/<?=$value->imagem?>" alt="<?=$value->titulo?>">
					</a>
				</li>				
			<?php endforeach ?>
		<?php endif; ?>
	</ul>
</div>