  <div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?if(!isset($registro[0])):?><a href="javascript: $('#form-ins').slideToggle('normal'); return false;" class="btn btn-success">Adicionar Imagem</a><?endif;?> <div style="max-width:700px;"><?=$titulo?></div>
    </h2>    
  </div>

  <a href="painel/blog/index" class="btn">← voltar</a>

  <br><br>

<?if(isset($registro[0])):?>

	<form method="post" id="form-alt" action="<?=base_url('painel/'.$this->router->class.'/imagensAlterar/'.$registro[0]->id)?>" enctype="multipart/form-data">

		<h3>Alterar Imagem</h3>

		<label>Imagem<br>
			<a target="_blank" href="_imgs/blog/<?=$registro[0]->imagem?>"><img src="_imgs/blog/thumbs/<?=$registro[0]->imagem?>"></a><br>
		<input type="file" name="userfile"></label>

		<label>
			Legenda<br>
			<textarea name="legenda" class="medio basico"><?=$registro[0]->legenda?></textarea>
		</label>

		<input type="hidden" name="id_post" value="<?=$id_post?>">

		<div class="form-actions">
			<button class="btn btn-primary" type="submit">Salvar</button>			
		</div>

	</form>

<?else:?>

	<form method="post" id="form-ins" action="<?=base_url('painel/'.$this->router->class.'/imagensInserir')?>" enctype="multipart/form-data">

		<h3>Inserir Imagem</h3>

		<label>Imagem<br>			
		<input type="file" name="userfile" required></label>

		<label>
      Legenda<br>
      <textarea name="legenda" class="grande basico"></textarea>
    </label>

		<input type="hidden" name="id_post" value="<?=$id_post?>">

		<div class="form-actions">
			<button class="btn btn-primary" type="submit">Salvar</button>			
		</div>

	</form>

<?endif;?>

<br><br>

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="blog_imagens">

          <thead>
            <tr>
            	<th>Ordenar</th>
            	<th class="yellow header headerSortDown">Imagem</th>
            	<th class="header">Legenda</th>
            	<th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                	<td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
                	<td><img src="_imgs/blog/thumbs/<?=$value->imagem?>"></td>
                	<td><?=$value->legenda?></td>
                  	<td class="crud-actions" style="width:118px; text-align:center;">
                  		<a href="painel/<?=$this->router->class?>/imagens/<?=$id_post.'/'.$value->id?>" class="btn btn-primary">editar</a>
                    	<a href="painel/<?=$this->router->class?>/imagensExcluir/<?=$value->id.'/'.$id_post?>" class="btn btn-danger btn-delete">excluir</a>
                  	</td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

      <?php else:?>

        <h2>Nenhuma Imagem para o Post</h2>

      <?php endif ?>
    </div>
  </div>
