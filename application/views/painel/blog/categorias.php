<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?> <a  href="painel/<?=$this->router->class?>/categoriasForm" class="btn btn-success">Adicionar <?=$unidade?></a>
    </h2>
  </div>  

<div class="row">
    <div class="span12 columns">

    <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="blog_categorias">

          	<thead>
            	<tr>
              		<th>Ordenar</th>
              		<th class="yellow header headerSortDown">Título</th>
              		<th class="red header">Ações</th>
            	</tr>
          	</thead>

          	<tbody>
	            <?php foreach ($registros as $key => $value): ?>
	              
	                <tr class="tr-row" id="row_<?=$value->id?>">
	                	<td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
	                  	<td class="nowrap"><?=$value->titulo?></td>
	                  	<td class="crud-actions" style="width:150px;">
		                    <a href="painel/<?=$this->router->class?>/categoriasForm/<?=$value->id?>" class="btn btn-primary">editar</a>
		                    <a href="painel/<?=$this->router->class?>/categoriasExcluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
	                  	</td>
	                </tr>

	            <?php endforeach ?>
          	</tbody>

        </table>

    <?php else:?>

    	<h2>Nenhuma Categoria Cadastrada</h2>

    <?php endif ?>

    </div>
</div>