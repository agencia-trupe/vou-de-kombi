<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Texto<br>
		<input type="text" name="texto" required autofocus value="<?=$registro->texto?>"></label>

		<label>Caixa<br>
			<select name="caixa" required>
				<option value="1" <?if($registro->caixa)echo" selected"?>>Azul à esquerda</option>
				<option value="2" <?if($registro->caixa)echo" selected"?>>Preta à direita</option>
			</select>
		</label>	

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Texto<br>
		<input type="text" name="texto" required autofocus></label>

		<label>Caixa<br>
			<select name="caixa" required>
				<option value="1">Azul à esquerda</option>
				<option value="2">Preta à direita</option>
			</select>
		</label>	

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>