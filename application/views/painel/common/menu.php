<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container">

        <a href="painel/home" class="brand">Vou de Kombi</a>

        <ul class="nav">

            <li <?if($this->router->class=='home')echo" class='active'"?>><a href="painel/home">Início</a></li>

            <li class="dropdown  <?if($this->router->class=='banners'||$this->router->class=='chamadas'||$this->router->class=='frases')echo"active"?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="painel/banners/">Imagens</a></li>
                    <li><a href="painel/chamadas/">Chamadas</a></li>
                    <li><a href="painel/frases/">Frases</a></li>
                </ul>
            </li>

            <li <?if($this->router->class=='galeria')echo" class='active'"?>><a href="painel/galeria">Galeria</a></li>

            <li class="dropdown  <?if($this->router->class=='blog')echo"active"?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Diário de Viagem <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="painel/blog/categorias">Categorias</a></li>
                    <li><a href="painel/blog/">Posts</a></li>            
                </ul>
            </li>

            <li class="dropdown  <?if($this->router->class=='roteiro')echo"active"?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Roteiro <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="painel/roteiro/index">Próximos Destinos</a></li>
                    <li><a href="painel/roteiro/passamos">Já Passamos</a></li>
                    <!-- <li><a href="painel/roteiro/googlemaps">Arquivo KML do Google Maps</a></li> -->
                </ul>
            </li>

            <li <?if($this->router->class=='apoiadores')echo" class='active'"?>><a href="painel/apoiadores">Apoiadores</a></li>

            <li class="dropdown <?if($this->router->class=='usuarios')echo"active"?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="painel/usuarios">Usuários</a></li>
                    <li><a href="painel/home/logout">Logout</a></li>
                </ul>
            </li>

        </ul>

    </div>
  </div>
</div>