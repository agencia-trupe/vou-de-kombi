<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>País<br>
		<select name="pais" required>
			<option value="">selecione o país</option>
			<?php foreach ($paises as $key => $value): ?>
				<option value="<?=$value->iso?>" <?if($value->iso==$registro->pais)echo" selected"?>><?=$value->nome?></option>
			<?php endforeach ?>
		</select></label><br>

		<label>Cidade<br>
		<input type="text" name="cidade" required value="<?=$registro->cidade?>"></label><br>

		<label>Data de Chegada<br>
		<input type="text" name="data_chegada" class="datepicker" required value="<?=formataData($registro->data_chegada,'mysql2br')?>"></label><br>

		<label>Descritivo<br>
		<textarea name="descritivo" class="medio basico"><?=$registro->descritivo?></textarea></label><br>
		
		Imagem<br>
			<?if($registro->imagem):?>
				<img src="_imgs/roteiro/<?=$registro->imagem?>"><br>
				<input type="checkbox" name="remover_imagem" value="1"> Remover Imagem
				<br><br>
			<?endif;?>
		<label><input type="file" name="userfile"></label><br>

		<label>Linkar com entrada no Diário<br>
		<select name="slug_blog" style="width:600px;">
			<option value="">- selecione uma entrada no diário -</option>
			<?php if ($diario): ?>
				<?php foreach ($diario as $key => $value): ?>
					<option value="<?=$value->slug?>" <?if($value->slug==$registro->slug_blog)echo" selected"?>><?=$value->titulo?></option>
				<?php endforeach ?>
			<?php endif ?>
		</select>
		</label><br>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>País<br>
		<select name="pais" required>
			<option value="">selecione o país</option>
			<?php foreach ($paises as $key => $value): ?>
				<option value="<?=$value->iso?>"><?=$value->nome?></option>
			<?php endforeach ?>
		</select></label>

		<label>Cidade<br>
		<input type="text" name="cidade" required></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>