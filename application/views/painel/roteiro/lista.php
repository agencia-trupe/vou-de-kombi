<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?> <?php if($this->router->method=='index'):?><a href="painel/<?=$this->router->class?>/form" class="btn btn-success">Adicionar <?=$unidade?></a><?php endif;?>
    </h2>
  </div>  

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable">

          <thead>
            <tr>
              <th class="yellow header headerSortDown">País</th>
              <th class="header">Cidade</th>
              <?php if ($this->router->method=='passamos'): ?>
                <th class="red header">Descrição</th>                  
              <?php endif ?>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">

                    <td style="white-space:nowrap;"><?=nomePais($value->pais)?></td>
                    <td style="white-space:nowrap;"><?=$value->cidade?></td>

                    <?php if ($this->router->method=='passamos'): ?>
                        <td><?=word_limiter($value->descritivo, 15)?></td>
                        <td class="crud-actions">    
                            <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                            <a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                        </td>
                    <?php else: ?>
                        <td class="crud-actions" style="width:153px;">    
                            <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">Chegamos!</a>
                            <a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                        </td>
                    <?php endif ?>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

        <?php if ($paginacao): ?>          
          <div class="pagination">
            <ul>        
              <?=$paginacao?>
            </ul>
          </div>
        <?php endif ?>

      <?php else:?>

      	<h3>Nenhum Registro</h2>

      <?php endif ?>

    </div>
  </div>