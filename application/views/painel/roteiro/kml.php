    <div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert">Arquivo Atualizado!</div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert">Erro ao atualizar o arquivo!</div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            <a href="javascript: $('#form-kml').slideToggle('normal'); return false;" class="btn btn-success">Atualizar Arquivo</a><div style="max-width:700px;">Arquivo KML do Google Maps</div>
        </h2>    
    </div>

    <a href="painel/blog/index" class="btn">← voltar</a>

    <br><br>

	<form method="post" class="hidden" id="form-kml" style="display:none;" action="<?=base_url('painel/'.$this->router->class.'/atualizaKml')?>" enctype="multipart/form-data">

    	<h3>Atualizar Arquivo .kml</h3>

		<label>Arquivo<br>
        <input type="file" name="userfile"></label>

		<div class="form-actions">
			<button class="btn btn-primary" type="submit">Salvar</button>			
		</div>

	</form>

    <br><br>

    <div class="row">
        <div class="span12 columns">

        Arquivo Atual:<br><br>
        <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps/ms?msa=0&amp;msid=201812029968633018671.0004ddc7bfcbbcf97f661&amp;ie=UTF8&amp;t=m&amp;ll=-22.209297,-68.503036&amp;spn=65.21199,43.718323&amp;output=embed"></iframe><br /><small>Visualizar <a href="https://maps.google.com/maps/ms?msa=0&amp;msid=201812029968633018671.0004ddc7bfcbbcf97f661&amp;ie=UTF8&amp;t=m&amp;ll=-22.209297,-68.503036&amp;spn=65.21199,43.718323&amp;source=embed" style="color:#0000FF;text-align:left">Vou de Kombi</a> em um mapa maior</small>

        </div>
    </div>
