<div class="corpo">
	
	<h1 class="fit"><a href='roteiro' title='voltar para roteiro'>ROTEIRO<img src='_imgs/layout/seta-voltar.png' alt="seta de voltar"></a></h1>

	<h2 class="amarelo">ÁLBUM</h2>

	<div class="f">
		<h3>POR ONDE JÁ ANDAMOS...</h3>		
		<div class="caixa">
			<div class="titulo"><?=nomePais($detalhe->pais).' | '.$detalhe->cidade?></div>
			<div class="data">chegada: <?=formataData($detalhe->data_chegada, 'custom2')?></div>
			<div class="imagem">
				<?php if ($anterior): ?>
					<a id="prev" href="roteiro/album/<?=$anterior->id?>" title="Roteiro Anterior">></a>					
				<?php endif ?>
				<?php if ($detalhe->imagem): ?>
					<img src="_imgs/roteiro/<?=$detalhe->imagem?>" alt="<?=strip_tags($detalhe->descritivo)?>">
				<?php endif ?>
				<?php if ($proximo): ?>
					<a id="next" href="roteiro/album/<?=$proximo->id?>" title="Pŕoximo Roteiro"><</a>					
				<?php endif ?>
			</div>
			<div class="descritivo"><?=$detalhe->descritivo?></div>
		</div>
		<?php if ($detalhe->slug_blog): ?>
			<a class="link-diario" href="diario/ler/<?=$detalhe->slug_blog?>">veja o diário de viagem da chegada &raquo;</a>
		<?php endif ?>
	</div>

	<ul id="navegacao-paises">
		<?php if ($lista_paises): ?>				
			<?php foreach ($lista_paises as $key => $paises): ?>
				<li><a href="roteiro/album/<?=$paises->link->id?>" <?if($paises->pais==$detalhe->pais)echo" class='ativo'"?> title="<?=nomePais($paises->pais)?>">* <?=mb_strtoupper(nomePais($paises->pais))?></a></li>
			<?php endforeach ?>
		<?php endif ?>
	</ul>

</div>