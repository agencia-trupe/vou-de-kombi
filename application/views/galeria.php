<div class="corpo noscroll">

	<a href="#" class="galeria-prev" title="Foto Anterior">&laquo;</a>

	<div id="slides-home" class="galeria">
		<?php if ($imagens): ?>
			<?php foreach ($imagens as $key => $value): ?>
				<div class="slide" style="background-image:url('_imgs/galeria/<?=$value->imagem?>')"></div>				
			<?php endforeach ?>
		<?php endif ?>
	</div>
	
	<a href="#" class="galeria-next" title="Próxima Foto">&raquo;</a>

</div>