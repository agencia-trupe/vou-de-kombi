<div class="corpo">
	
	<h1 class="fit">FALE CONOSCO</h1>

	<div class="social">
		<a href="http://www.facebook.com/kombialice" target="_blank" class="link-facebook" title="Facebook">Facebook</a>
		<a href="https://twitter.com/VoudeKombi" target="_blank" class="link-twitter" title="Twitter">Twitter</a>
		<a href="https://vimeo.com/user17178089" target="_blank" class="link-vimeo" title="Vimeo">Vimeo</a>
		<a href="" target="_blank" class="link-youtube" title="Youtube">Youtube</a>
	</div>

	<p class="padrao">
		Fale conosco através do Facebook:<br> <a href="https://www.facebook.com/kombialice" class="link-contato" title="Nossa página no Facebook">facebook.com/kombialice</a>
	</p>

	<p class="padrao">
		ou via e-mail:<br><a href="mailto:voudekombipelomundo@gmail.com" class="link-contato" title="Envie um e-mail!">voudekombipelomundo@gmail.com</a>
	</p>

</div>