

    <div id="assinatura" class="interna">
        <a href="http://www.trupe.net" target="_blank" title="Criação de Sites: Trupe Agência Criativa"><span>Criação de Sites: Trupe Agência Criativa</span> <img src="_imgs/layout/trupe.png" alt="Trupe Agência Criativa"></a>
    </div>

    </div> <!-- fim da div main -->

    <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
        <script>
            window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
            Modernizr.load({
                load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
            });
        </script>
    <?endif;?>

    <?JS(array('fancybox','fittext/jquery.fittext','infinite-scroll/jquery.infinitescroll.min','cycle','front.min'))?>

</body>
</html>
