<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Vou de Kombi</title>
    <meta name="description" content="Vou de Kombi é um projeto de cunho antropológico, cultural, social, artístico, ambiental e ecológico. Nele, o casal de fotógrafos documentaristas percorrerão mais de 200.000km em uma Kombi adaptada em casa-escritório por países dos 4 continentes (Americano, Europeu, Asiático e Africano) em aproximadamente 5 anos.">
    <meta name="keywords" content="" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2012 Trupe Design" />

    <meta name="viewport" content="width=device-width,initial-scale=1">

    <meta property="og:title" content="Vou de Kombi"/>
    <meta property="og:site_name" content="Vou de Kombi"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="<?=base_url('_imgs/layout/fb.jpg')?>"/>
    <meta property="og:url" content="<?=base_url()?>"/>
    <meta property="og:description" content="Vou de Kombi é um projeto de cunho antropológico, cultural, social, artístico, ambiental e ecológico. Nele, o casal de fotógrafos documentaristas percorrerão mais de 200.000km em uma Kombi adaptada em casa-escritório por países dos 4 continentes (Americano, Europeu, Asiático e Africano) em aproximadamente 5 anos."/>

    <base href="<?= base_url() ?>">
    <script> var BASE = '<?= base_url() ?>'</script>

    <?CSS(array('reset', 'base', 'fontface/stylesheet', 'fancybox/fancybox', $this->router->class, $load_css))?>

    <?if(ENVIRONMENT == 'development'):?>
    
        <?JS(array('modernizr-2.0.6.min', 'less-1.3.3.min', 'jquery-1.8.0.min', $this->router->class, $load_js))?>
    
    <?else:?>

        <?JS(array('modernizr-2.0.6.min', $this->router->class, $load_js))?>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

    <?endif;?>

</head>
<body class="body-<?=$this->router->class?> body-<?=$this->router->class?>-<?=$this->router->method?>">