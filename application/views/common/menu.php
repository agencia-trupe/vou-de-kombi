<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

	<header>

		<a href="home" title="Página Inicial" id="link-home"><img src="_imgs/layout/marca-voudekombi.png" alt="Logo Vou de Kombi"></a>

		<nav>
			<ul>
				<li><a href="home" title="Página Inicial" id="mn-home" <?if($this->router->class=='home')echo" class='ativo'"?>>home</a></li>
				<li><a href="galeria" title="Galeria de Fotos" id="mn-galeria" <?if($this->router->class=='galeria')echo" class='ativo'"?>>galeria</a></li>
				<li><a href="diario-de-viagem" title="Nosso Diário de Viagem" id="mn-diario" <?if($this->router->class=='diario')echo" class='ativo'"?>>diário de viagem</a></li>
				<li><a href="roteiro" title="O Roteiro de Nossa Viagem" id="mn-roteiro" <?if($this->router->class=='roteiro')echo" class='ativo'"?>>roteiro</a></li>
				<li><a href="projeto" title="O Projeto Vou de Kombi" id="mn-projeto" <?if($this->router->class=='projeto')echo" class='ativo'"?>>o projeto</a></li>
				<li><a href="apoiadores" title="Apoiadores" id="mn-companheiros" <?if($this->router->class=='apoiadores')echo" class='ativo'"?>>apoiadores</a></li>
				<li><a href="contato" title="Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>contato</a></li>
			</ul>
		</nav>

	    <div id="assinatura" class="home">
            <a href="http://www.trupe.net" target="_blank" title="Criação de Sites: Trupe Agência Criativa"><span>Criação de Sites: Trupe Agência Criativa</span> <img src="_imgs/layout/trupe.png" alt="Trupe Agência Criativa"></a>
        </div>

	</header>

	<div id="header-spacer"></div>