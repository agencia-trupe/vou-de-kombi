<div class="corpo noscroll">

	<div class="bgazul">

		<div class="social">
			<a href="http://www.facebook.com/kombialice" target="_blank" class="link-facebook" title="Facebook">Facebook</a>
			<a href="https://twitter.com/VoudeKombi" target="_blank" class="link-twitter" title="Twitter">Twitter</a>
			<a href="https://vimeo.com/user17178089" target="_blank" class="link-vimeo" title="Vimeo">Vimeo</a>
			<a href="" target="_blank" class="link-youtube" title="Youtube">Youtube</a>
		</div>

		<div class="chamadas-fixas">
			<a href="app" title="Baixe nosso App" class="link-app"><img src="_imgs/layout/download-app.png" alt="Baixe nosso App"><div class="texto">Faça o download do nosso aplicativo para iOS e Android!</div></a>
			<a href="projeto/alice" title="Conheça a Alice" class="link-alice"><div class="texto">Saiba mais sobre a história da Alice, a nossa Kombi</div><img src="_imgs/layout/icone-alice.png" alt="Conheça a Alice"></a>
		</div>

		<div class="chamadas">
			<span class="titulo-chamadas">CONFIRA:</span><br>
			<?php if ($chamadas): ?>
				<?php foreach ($chamadas as $key => $value): ?>
					<a href="<?=$value->destino?>" class="chamada" title="<?=$value->titulo?>"><span><?=$value->titulo?></span></a>
				<?php endforeach ?>				
			<?php endif ?>
		</div>

	</div>

	<div id="slides-home">
		<?php if ($banners): ?>
			<?php foreach ($banners as $key => $value): ?>
				<div class="slide" style="background-image:url('_imgs/home/<?=$value->imagem?>')"></div>				
			<?php endforeach ?>
		<?php endif ?>
	</div>

	<div class="frases-preto">
		<?php if ($frases_pretas): ?>
			<?php foreach ($frases_pretas as $key => $value): ?>
				<div class="frase" style="right:0;<?php if($key>0)echo "display:none;"?>"><span><?=$value->texto?></span></div>
			<?php endforeach ?>
		<?php endif ?>
	</div>

	<div class="frases-azul">
		<?php if ($frases_azuis): ?>
			<?php foreach ($frases_azuis as $key => $value): ?>
				<div class="frase"><?=$value->texto?></div>
			<?php endforeach ?>
		<?php endif ?>
	</div>

</div>