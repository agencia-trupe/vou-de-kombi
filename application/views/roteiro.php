<div class="corpo">

	<h1 class="fit">roteiro</h1>

	<iframe style="margin:0;border:0;width:61%;height:550px;" scrolling="no" src="https://maps.google.com/maps/ms?msa=0&amp;msid=201812029968633018671.0004ddc7bfcbbcf97f661&amp;ie=UTF8&amp;ll=-22.209297,-68.503036&amp;spn=65.21199,43.718323&amp;t=m&amp;output=embed"></iframe>

	<aside>

		<?php if ($atual): ?>
			<a href="diario/ler/<?=$atual->slug_blog?>" title="<?=word_limiter(strip_tags($atual->descritivo), 5)?>" class="link-atual">
				<h2>ONDE ESTAMOS?</h2>
				<div class="azul">
					<h3><?=mb_strtoupper(nomePais($atual->pais))?>&nbsp;|&nbsp;<?=$atual->cidade?></h3>
					<span class="data">chegada: <?=formataData($atual->data_chegada,'mysql2br')?></span>
					<?php if ($atual->imagem): ?>
						<img src="_imgs/roteiro/<?=$atual->imagem?>" alt="<?=$atual->descritivo?>">
					<?php endif ?>
					<?=$atual->descritivo?>
				</div>
				<div class="aright">veja o diário de viagem da chegada &raquo;</div>
			</a>
		<?php endif ?>

		<?php if ($proximos): ?>
			<div class="proximos">
				<h2>PRÓXIMOS DESTINOS:</h2>
				<ul>
					<?php foreach ($proximos as $key => $value): ?>
						<li>* <?=mb_strtoupper(nomePais($value->pais))?>&nbsp;|&nbsp;<?=$value->cidade?></li>
					<?php endforeach ?>
				</ul>
			</div>
		<?php endif ?>

	</aside>

	<?php if ($passamos): ?>
		<div class="passamos">
			<h2>POR ONDE JÁ ANDAMOS... <a href="roteiro/album" title="VER ÁLBUM AMPLIADO">VER ÁLBUM AMPLIADO</a></h2>
			<ul>
				<?php foreach ($lista_paises as $key => $paises): ?>
					<li><a href="roteiro/pais/<?=$paises->pais?>" data-pais="<?=$paises->pais?>" title="<?=nomePais($paises->pais)?>">* <?=mb_strtoupper(nomePais($paises->pais))?></a></li>
				<?php endforeach ?>
			</ul>
			<div class="thumbs">
				<?php foreach ($passamos as $key => $value): ?>
					<a data-pais="<?=$value->pais?>" href="roteiro/album/<?=$value->id?>" title="<?=word_limiter(strip_tags($value->descritivo), 10)?>">
						<div class="imagem">
							<?php if ($value->imagem): ?>
								<img src="_imgs/roteiro/thumbs/<?=$value->imagem?>" alt="<?=word_limiter(strip_tags($value->descritivo), 10)?>">
								<div class="overlay showup hidden">
									<div class="cidade"><?=$value->cidade?></div>
									veja o diário de viagem da chegada &raquo;
								</div>
							<?php else: ?>
								<div class="overlay">
									<div class="cidade"><?=$value->cidade?></div>
									veja o diário de viagem da chegada &raquo;
								</div>
							<?php endif ?>
						</div>
						<?=formataData($value->data_chegada, 'custom2')?> &bull; <?php echo nomePais($value->pais) ?>
					</a>
				<?php endforeach ?>
			</div>
		</div>
	<?php endif ?>


</div>