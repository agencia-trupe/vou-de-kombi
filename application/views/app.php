<div class="corpo azul">
	
	<h1 class="apptit"><img src="_imgs/layout/download-app.png" alt="Download app">Faça o download do nosso aplicativo para iOS e Android!</h1>

	<div class="links">
		<a href="https://play.google.com/store/apps/details?id=br.com.moblee.voudekombi" title="Baixe o aplicativo para Android" target="_blank" class="android" style="margin-bottom:20px">
			<img src="_imgs/layout/download-android.png" alt="App para Android">
		</a>
		<a href="https://itunes.apple.com/br/app/vou-de-kombi/id649028082?mt=8" title="Baixe o aplicativo para iOS" target="_blank" class="ios" style="margin-bottom:20px">
			<img src="_imgs/layout/download-ios.png" alt="App para iOS">
		</a>
	</div>

</div>