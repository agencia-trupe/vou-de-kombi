<div class="corpo">

	<h1 class="fit">nosso diário de viagem</h1>
	
	<?php if ($query): ?>

		<div class="post">

			<div class="texto">

				<div class="data">
					<?=formataData($query->data, 'custom')?>
				</div>

				<h2>
					<?=$query->titulo?>
				</h2>

				<div class="categoria">
					<?=categoria($query->id_blog_categorias)?>
				</div>

				<?php if ($imagens): ?>
					<?php foreach ($imagens as $key => $value): ?>

						<img src="_imgs/blog/<?=$value->imagem?>" alt="<?=$query->titulo?>">
						<?=$value->legenda?>

					<?php endforeach ?>
				<?php endif ?>

			</div>

			<?php if ($paginacao): ?>
				<div class="paginacao-posts"><?php echo $paginacao ?></div>
			<?php endif ?>

			<?php if ($comentarios): ?>
				<div id="comentarios">
					<h2>COMENTÁRIOS</h2>
					<?php foreach ($comentarios as $key => $value): ?>
						<div class="comentario">
							<h3><?=$value->nome?> <span class="data"><?=formataData($value->data, 'mysql2br')?></span> </h3>
							<p><?=$value->comentario?></p>
						</div>
					<?php endforeach ?>
				</div>
			<?php endif ?>

			<div id="comentarios-form">
				<h2>COMENTE!</h2>
				<form action="diario/comentar" method="post" id="form-comentario">
					<div class="coluna">
						<input type="text" name="nome" placeholder="NOME">
						<input type="email" name="email" placeholder="E-MAIL">
						<input type="hidden" name="post" value="<?=$query->id?>">
						<input type="submit" value="ENVIAR">
					</div>
					<div class="coluna-maior">
						<textarea name="mensagem" placeholder="MENSAGEM"></textarea>
						<textarea name="recomendacoes" id="input-email"></textarea>
					</div>
				</form>
			</div>

		</div>

		<aside>
			<ul id="cat_recentes">
			<?php $catCont = 0; ?>
			<?php foreach ($categorias_recentes as $key => $value): ?>
				<?php if ($key <= 2): ?>
					<li><a href="diario/categoria/<?=$value->slug?>" title="<?=$value->titulo?>"><?=$value->titulo?></a></li>
				<?php else:?>
					<li class="hid"><a href="diario/categoria/<?=$value->slug?>" title="<?=$value->titulo?>"><?=$value->titulo?></a></li>
				<?php endif ?>
				<?php $catCont++; ?>
			<?php endforeach ?>
			</ul>
			<?php if ($catCont > 3): ?>				
				<a href="#" title="Ver todas as categorias" class="abre-categorias">VER TODAS AS CATEGORIAS &raquo;</a>
			<?php endif ?>

			<?php if ($post_recente): ?>
				<h3>próximo diário :</h3>
				<a href="diario/ler/<?=$post_recente->slug?>" class='thumb-post' title="<?=$post_recente->titulo?>">
					<?php if ($post_recente->thumb): ?>
						<img src="_imgs/blog/thumbs/<?=$post_recente->thumb->imagem?>" alt="<?=$post_recente->titulo?>">						
					<?php endif ?>
					<span class='data'><?=formataData($post_recente->data, 'custom')?></span>
					<div class="titulo"><?=$post_recente->titulo?></div>
				</a>
			<?php endif ?>

			<?php if ($tres_anteriores): ?>
				<h3>antes disso:</h3>
				<?php foreach ($tres_anteriores as $key => $value): ?>
					<a href="diario/ler/<?=$value->slug?>" class='thumb-post' title="<?=$value->titulo?>">
						<?php if ($value->thumb): ?>
							<img src="_imgs/blog/thumbs/<?=$value->thumb->imagem?>" alt="<?=$value->titulo?>">							
						<?php endif ?>
						<span class='data'><?=formataData($value->data, 'custom')?></span>
						<div class="titulo"><?=$value->titulo?></div>
					</a>
				<?php endforeach ?>
			<?php endif ?>
		</aside>

	<?php endif ?>

</div>