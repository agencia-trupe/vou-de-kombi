<div class="corpo">

	<h1 class="fit">nosso diário de viagem</h1>

	<?php if ($categoria[0]): ?>
		<h2 class="categoria"><?=$categoria[0]->titulo?></h2>
	<?php endif ?>
	
	<?php if ($posts): ?>
		<ul id="lista-posts">
		<?php foreach ($posts as $key => $value): ?>
			<li>
				<a href="diario/ler/<?=$value->slug?>" title="<?=$value->titulo?>">
					<div class="imagem">
						<?php if ($value->thumb): ?>
							<img src="_imgs/blog/thumbs/<?=$value->thumb->imagem?>" alt="<?=$value->titulo?>">							
						<?php endif ?>
					</div>
					<div class="data">
						<?=formataData($value->data,'custom')?>
					</div>
					<div class="titulo">
						<?=$value->titulo?>
					</div>
					<div class="barra">
						<?=$value->num_comentarios?>
						<span>comente e compartilhe!</span>
					</div>
				</a>
			</li>			
		<?php endforeach ?>
		</ul>
		<?php if (isset($paginacao) && $paginacao): ?>
			<div class="paginacao">
				<?php echo $paginacao ?>
			</div>
		<?php endif ?>
	<?php endif ?>

</div>