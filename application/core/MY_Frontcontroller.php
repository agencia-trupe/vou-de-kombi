<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Frontcontroller extends CI_controller {

    var $headervar;
    var $footervar;
    var $menuvar;
    var $hasLayout;

    function __construct($css = '', $js = '') {
        parent::__construct();
        
        $this->headervar['load_css'] = $css;
        $this->headervar['load_js'] = $js;
        //$this->output->enable_profiler(TRUE);
        $this->hasLayout = TRUE;
    }
    
    function _output($output){
        if ($this->hasLayout == FALSE || $this->input->is_ajax_request()) {
            echo $output;
        } else {
            
            $header = $this->_minify($this->load->view('common/header', $this->headervar, TRUE));
            $menu = $this->_minify($this->load->view('common/menu', $this->menuvar, TRUE));
            $body = $this->_minify($output);
            $footer = $this->_minify($this->load->view('common/footer', $this->footervar, TRUE));

            echo $header.$menu.$body.$footer;
        }        
    }

    function _minify($str){
        $str = str_replace(PHP_EOL, ' ', $str);
        $str = preg_replace('/[\r\n]+/', "\n", $str);
        $str = preg_replace('/[ \t]+/', ' ', $str);
        return $str;
    }

}
?>