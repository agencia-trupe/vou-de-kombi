<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frases_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'home_frases';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array('texto', 'caixa');
		$this->dados_tratados = array();
	}
}