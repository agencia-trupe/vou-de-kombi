<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roteiro_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'roteiro';
		//$this->tabela_imagens = 'tabela_imagens';

		// $this->dados = array('data_divulgacao', 'data', 'titulo', 'descritivo', 'link', 'imagem');
		// $this->dados_tratados = array(
		// 	'data' => formataData($this->input->post('data'), 'br2mysql'),
		// 	'imagem' => $this->sobeImagem()
		// );
	}

	function anterior($id_atual){
		$qry_atual = $this->pegarPorId($id_atual);
		$q = $this->db->query("SELECT * FROM roteiro WHERE data_chegada IS NOT NULL AND data_chegada < '".$qry_atual->data_chegada."' ORDER BY data_chegada DESC LIMIT 0,1")->result();
		return (isset($q[0])) ? $q[0] : false;
	}

	function proximo($id_atual){
		$qry_atual = $this->pegarPorId($id_atual);
		$q = $this->db->query("SELECT * FROM roteiro WHERE data_chegada IS NOT NULL AND data_chegada > '".$qry_atual->data_chegada."' ORDER BY data_chegada ASC LIMIT 0,1")->result();
		return (isset($q[0])) ? $q[0] : false;
	}

	function maisRecentePorPais($pais){
		$qry = $this->db->query("SELECT * FROM roteiro WHERE pais = '".$pais."' ORDER BY data_chegada DESC LIMIT 0,1")->result();
		return (isset($qry[0])) ? $qry[0] : false;
	}


	function inserir(){
		return $this->db->set('pais', $this->input->post('pais'))
						->set('cidade', $this->input->post('cidade'))
						->insert($this->tabela);
	}

	function alterar($id){
		$imagem = $this->sobeImagem();
		$this->db->set('data_chegada', formataData($this->input->post('data_chegada'), 'br2mysql'))
				 ->set('descritivo', $this->input->post('descritivo'))				 
				 ->set('slug_blog', $this->input->post('slug_blog'))
				 ->set('chegamos', 1)
				 ->where('id', $id);

		if ($imagem)
			$this->db->set('imagem', $imagem);
		
		if($this->input->post('remover_imagem') == 1)
			$this->db->set('imagem', '');

		return $this->db->update($this->tabela);
	}

	function pegarTodos($chegamos, $order_campo = 'id', $order = 'ASC'){
		if ($chegamos)
			return $this->db->order_by('data_chegada', 'DESC')->order_by('pais', 'ASC')->order_by('cidade', 'ASC')->get_where($this->tabela, array('chegamos' => $chegamos))->result();
		else
			return $this->db->order_by('id', 'ASC')->order_by('pais', 'ASC')->order_by('cidade', 'ASC')->get_where($this->tabela, array('chegamos' => $chegamos))->result();
	}
	function pegarPaginado($chegamos, $por_pagina, $inicio, $order_campo = 'id', $order = 'DESC'){
		if ($chegamos)
			return $this->db->order_by('data_chegada', 'DESC')->order_by('pais', 'ASC')->order_by('cidade', 'ASC')->get_where($this->tabela, array('chegamos' => $chegamos), $por_pagina, $inicio)->result();
		else
			return $this->db->order_by('id', 'ASC')->order_by('pais', 'ASC')->order_by('cidade', 'ASC')->get_where($this->tabela, array('chegamos' => $chegamos), $por_pagina, $inicio)->result();
	}
	function numeroResultados($chegamos){
		return $this->db->get_where($this->tabela, array('chegamos' => $chegamos))->num_rows();
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');
		
		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/roteiro/'
		);
		$campo = $original['campo'];
	
		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');
	
		$this->upload->initialize($uploadconfig);
	
		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
	
	        	 $this->image_moo
	        	 		->load($original['dir'].$filename)
	         	  		->resize(800, 860)
	         	  		->save($original['dir'].$filename, TRUE)
	         	  		->resize_crop(150,100)
	         	  		->save($original['dir'].'thumbs/'.$filename, TRUE);
	
		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	
}