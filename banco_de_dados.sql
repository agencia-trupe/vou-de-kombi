-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 03-Jun-2013 às 09:45
-- Versão do servidor: 5.1.68-cll
-- versão do PHP: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `kombi_site`
--
use `voudekombi`;
-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) DEFAULT NULL,
  `imagem` varchar(140) DEFAULT NULL,
  `ordem` int(11) DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `titulo`, `imagem`, `ordem`) VALUES
(5, 'Banner 1 ', 'slide11.jpg', 0),
(6, 'Banner 2', 'slide21.jpg', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) NOT NULL,
  `data` datetime NOT NULL,
  `id_blog_categorias` int(10) unsigned NOT NULL,
  `slug` varchar(140) NOT NULL,
  `data_postagem` datetime DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  `autor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Extraindo dados da tabela `blog`
--

INSERT INTO `blog` (`id`, `titulo`, `data`, `id_blog_categorias`, `slug`, `data_postagem`, `data_alteracao`, `autor`) VALUES
(32, 'Aguardando nossa partida que acontecerá amanhã!', '2013-05-24 18:34:41', 1, 'aguardando_nossa_partida_que_acontecera_amanha', NULL, '2013-05-24 18:34:41', NULL),
(33, 'Festa de despedida ', '2013-05-24 19:20:08', 1, 'festa_de_despedida__1', '2013-05-24 19:19:26', '2013-05-24 19:20:08', 14),
(34, 'Nosso adeus a São Paulo', '2013-05-25 10:02:15', 1, 'nosso_adeus_a_sao_paulo_1', '2013-05-26 09:51:00', '2013-05-26 10:02:15', 14),
(35, 'De São Paulo para Goioerê', '2013-05-26 10:49:42', 1, 'de_sao_paulo_para_goioere', '2013-05-26 10:49:42', NULL, 14);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_categorias`
--

DROP TABLE IF EXISTS `blog_categorias`;
CREATE TABLE IF NOT EXISTS `blog_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) NOT NULL,
  `slug` varchar(140) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `data_ultima_inclusao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `blog_categorias`
--

INSERT INTO `blog_categorias` (`id`, `titulo`, `slug`, `ordem`, `data_ultima_inclusao`) VALUES
(1, 'BRASIL', 'brasil', -1, '2013-05-29 04:54:17'),
(2, 'ONDE FICAR', 'onde_ficar_1', -1, '2013-05-29 16:33:57'),
(3, 'ONDE COMER', 'onde_comer', -1, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_comentarios`
--

DROP TABLE IF EXISTS `blog_comentarios`;
CREATE TABLE IF NOT EXISTS `blog_comentarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_blog` int(10) unsigned NOT NULL,
  `nome` varchar(140) NOT NULL,
  `email` varchar(140) NOT NULL,
  `comentario` text NOT NULL,
  `data` date NOT NULL,
  `datetime` datetime NOT NULL,
  `aprovado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Extraindo dados da tabela `blog_comentarios`
--

INSERT INTO `blog_comentarios` (`id`, `id_blog`, `nome`, `email`, `comentario`, `data`, `datetime`, `aprovado`) VALUES
(4, 32, 'Raro', 'rarodeoliveira@gmail.com', 'Boa viagem meus amigos, não estarei na festinha de despedida como da outra vez, mas estarei de coração e alma aí pertinho! Lá da vila da glória mando um torpedo com cheiro de mar.', '2013-05-24', '2013-05-24 22:14:38', 1),
(5, 32, 'Diego', 'diegoaraujo_only7@hotmail.com', 'Parabens amigos vou estar acompanhando a viagem inteira pena que não posso estar na festa de despedida mas estou acompanhando abraços.....', '2013-05-25', '2013-05-25 15:21:19', 1),
(6, 32, 'Simão Parada', 'simaoparada@terra.com.br', 'Parabéns pela iniciativa e valorização da liberdade de rumar por este mundão curtindo aquilo que nos da animo para viver...a aventura...Estarei acompanhando daqui de santa catarina sua viagem. ', '2013-05-25', '2013-05-25 20:38:00', 1),
(7, 34, 'ANA LIGIA CORADI', 'anacoradi@hotmail.com', 'meus queridos! foi muito bom passar lá dar um abraço em vcs e um tchau para nossa querida amiga... Que vocês façam uma belíssima viagem! que cada dia vocês aprendam algo novo. que cada lugar lhes presenteie com um amigo novo... e que na volta nós possamos tomar um chopp geladinho e ouvir muitas histórias! um abraço!\n', '2013-05-26', '2013-05-26 16:06:46', 1),
(8, 35, 'Jocemar, Adriana,Júlia e Miguel', 'familianaestrada@hotmail.com', 'Franco e Inês...Não os conhecemos pessoalmente, mas somos viajantes...o que nos torna amigos...Desejamos muita sorte, e ótimas experiências por este mundão ...A vida ta ai para ser vivida mesmo! Estamos aqui na Turquia, ancorado na beira do mar junto a novos  bons amigos, esperando o ferry para levar nós e nosso Papa Leguas para o Egito...Continuando nossa Expedição umja Família Pelo Mundo...Do que precisares estaremos sempre a disposição  com prazer...Boa viagem...Sendo Brasileiro, as portas se abrem pelo mundo, Quando me perguntam, qual meu país, eu respondo:moro no Brasil, O melhor país do Mundo...', '2013-05-27', '2013-05-27 06:38:42', 1),
(9, 34, 'carlos e roseli ', 'roscarcampos@hotmail.com', 'boa sorte companheiros  estamos torcendo  para que  seja  uma  viagem inesquecivel,  iremos  ajuda-los  e  acompanhar  atentamente em  todos  os  detalhes ,  pois  nosso  sonho  de  casamento  eh  esse   ter  nossa  kombi  e  viajarmos  mundo  a  fora  ,,,.....  vão  em   paz  e   alegria   \nabraços ', '2013-05-27', '2013-05-27 12:57:00', 1),
(10, 34, 'ALEX', 'alecsandro.dutra@hotmail.com', 'JÁ ESTOU CURTINDO ESSA VIAGEM', '2013-05-27', '2013-05-27 18:30:46', 1),
(11, 34, 'Evandro Monteiro', 'emontcar@gmail.com', 'Desejando sorte a voces e que deus proteja ambos.... boa viagem !', '2013-05-27', '2013-05-27 20:42:52', 1),
(12, 35, 'Cristian Barrueco Valenzuela', 'barrueco@gmail.com', 'Niños! Pena não ter ido na despedida, mas desejo de todo el corazón un feliz Viaje! \nMandem fotos!\n\nCuando pasen por Chile, nas cidades de Rancagua y San Francisco de Mostazal, lembrem-se dos chilenos patiperros aqui de Sampa! =D', '2013-05-27', '2013-05-27 20:56:15', 1),
(13, 34, 'Tatiane Neves Tavares', 'tatianenevestavares@gmail.com', 'Que Papai do céu guie o caminho de vocês e  que os guarde todas as noites, este projeto ja é um sucesso eu so tenho que desejar que  continue sendo e que volte com muiiiiiiiiiitas historias', '2013-05-27', '2013-05-27 21:09:32', 1),
(14, 35, 'Joé Rogério', 'j_rogerio@hotmail.com', 'Desejo à vocês uma excelênte expedição, uma ótima viagem, pena não poder estar junto com vocês no evento de despedia no EMPóRio, mas minha família e eu estamos orando, torcendo pelo sucesso de mas este desafio.\n\n', '2013-05-28', '2013-05-28 10:08:40', 1),
(15, 34, 'Fábio Tomaz', 'fabioebox@gmail.com', 'Inês e Franco, desejo muuuuuuuito sucesso para vocês nessa nova e promissora tarefa de levar ao mundo tudo aquilo que vocês têm de bom  para transmitir. Estarei acompanhando o trabalho de vocês com o maior prazer. A Alice ficou ainda mais charmosa com esse vestidinho azul! Hehehe! Aqui de Jundiaí, eu e minha família estamos enviando um beijo e um abraço enorme! Muita paz!', '2013-05-28', '2013-05-28 15:52:19', 1),
(16, 35, 'Voudekombi', 'ines.vcalixto@globo.com', 'José Rogério, agradecemos pelo carinho. Viajaremos sob o manto de suas orações. Grande beijo', '2013-05-29', '2013-05-29 16:04:30', 1),
(17, 35, 'VoudeKombi', 'ines.vcalixto@globo.com', 'Cristian Barrueco Valenzuela, sua cidade querida está em nosso roteiro, ao passar por ela nos lembraremos de vocês e enviaremos fotos. Grande abraço.', '2013-05-29', '2013-05-29 16:06:38', 1),
(18, 35, 'VoudeKombi', 'ines.vcalixto@globo.com', 'Obrigada Jocemar, Adriana, julia e miguel pelo carinho e pelas dicas valiosas. oxalá nossas rotas se cruzem um dia.', '2013-05-29', '2013-05-29 16:07:59', 1),
(19, 34, 'Franco & Inês', 'ines.vcalixto@globo.com', 'Fábio Tomaz! Amigo artista que fez a Alice em biscui, a linda pequena Alice! obrigada pelos votos de uma boa viagem. Ficamos super felizes por tê-lo acompanhando nosso novo projeto. Abraços à sua e nossa querida família de Jundiaí. Grande beijo,', '2013-05-29', '2013-05-29 16:12:35', 1),
(20, 34, 'Franco & Inês', 'ines.vcalixto@globo.com', 'Valeu, Tatiane! obrigada pela amizade, pelo apoio e carinho. Nós lhe desejamos toda a felicidade do mundo. Grande beijo', '2013-05-29', '2013-05-29 16:14:16', 1),
(21, 34, 'Franco & Inês', 'ines.vcalixto@globo.com', 'Valeu, Evandro. Brigadooo', '2013-05-29', '2013-05-29 16:15:01', 1),
(22, 34, 'Franco & Inês', 'ines.vcalixto@globo.com', 'Alex, estamos curtindo saber que vc curte nossa viagem. ', '2013-05-29', '2013-05-29 16:15:46', 1),
(23, 34, 'Franco & Inês', 'ines.vcalixto@globo.com', 'Carlos e Roseli, somos gratos pelo carinho. Desejamos que seu sonho de casamento se realize em breve. Grande beijo', '2013-05-29', '2013-05-29 16:17:23', 1),
(24, 34, 'Franco & Inês', 'ines.vcalixto@globo.com', 'Ana lígia, valeu! Obrogada pelo apoio e pela amizade sempre presente. Por todos os lugares por onde passarmos nos lembraremos de você. Grande beijo\n', '2013-05-29', '2013-05-29 16:18:39', 1),
(25, 32, 'Franco & Inês', 'ines.vcalixto@globo.com', 'Raro, recebemos o torpedo com cheiro de mar. Obrigado por sua amizade traduzida em gestos que sempre nos fizeram ver mais  longe. Grande beijo.', '2013-05-29', '2013-05-29 16:20:53', 1),
(26, 32, 'Franco & Inês', 'ines.vcalixto@globo.com', 'Valeu, Simão. Nossa viagem em busca da liberdade será também uma homenagem para você. Grande beijo.', '2013-05-29', '2013-05-29 16:23:26', 1),
(27, 32, 'Franco & Inês', 'ines.vcalixto@globo.com', 'Valeu, Diego. É uma alegria tê-lo acompanhando nossa viagem bem de pertinho. Nosso abraço carinhoso', '2013-05-29', '2013-05-29 16:24:26', 1),
(28, 35, 'Ricardo Bernardino Sena', 'ricsena@visaonet.com.br', 'Boa sorte!ao casal enos envie várias fotos , relatando paisagens, costumes e culturas dos locais por onde passarem...relamente acredito que será uma experiencia inexquecível!!!\nGood!! travel!!!', '2013-06-03', '2013-06-03 08:52:07', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `blog_imagens`
--

DROP TABLE IF EXISTS `blog_imagens`;
CREATE TABLE IF NOT EXISTS `blog_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(140) NOT NULL,
  `id_blog` int(10) unsigned NOT NULL,
  `legenda` text,
  `ordem` varchar(45) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Extraindo dados da tabela `blog_imagens`
--

INSERT INTO `blog_imagens` (`id`, `imagem`, `id_blog`, `legenda`, `ordem`) VALUES
(34, '971169_10200872028154913_1316743509_n.jpg', 32, '<p>Estamos ansiosos pela partida que acontecer&aacute; amanh&atilde;! Estamos hospedados no Transam&eacute;rica Prime e no s&aacute;bado, dia 25/05/2013 teremos nossa festa de despedida no Emp&oacute;rio Bendicta, a partir das 12h00. Venha nos prestigiar!</p>', '-1'),
(35, '946843_577265585640693_1043523550_n.jpg', 33, '<p>Festa de despedida "Volta ao mundo numa kombi em 1825 dias" no Emp&oacute;rio Bendicta &agrave; partir das 12hs.</p>\n<p>Rua Ilansa 130D - Mo&oacute;ca - (Travessa da Av. Paes de Barros, altura n&ordm; 3100)<br />S&atilde;o Paulo &ndash; SP - (11) 2061-8487 - <a href="http://www.emporiobendicta.com.br">www.emporiobendicta.com.br</a></p>', '-1'),
(36, '935473_532805793442838_807862390_n.jpg', 34, '<p>Uma Kombi entre Kombis, foi assim nossa despedida de S&atilde;o Paulo. Amigos, amantes da Kombi e da vida que viajava, apoiadores e patrocinadores nos encontraram no Emp&oacute;rio Bendicta para um abra&ccedil;o.&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '-1'),
(37, '972707_10151618085034921_1318962167_n.jpg', 34, '<p>Foi emocionante observar as express&otilde;es, os suspiros e sussurros quando os visitantes entravam dentro de nosso carro-casa. Era como se o mundo pudesse ser visto numa fra&ccedil;&atilde;o de segundos...</p>', '-1'),
(38, '969029_546284875410931_847756924_n.jpg', 34, '<p>Dentro do bar cerveja e prosa. Do que ser&aacute; que falavam? A gente mal entrou no Emp&oacute;rio...</p>', '-1'),
(39, '983613_532808360109248_1946517496_n.jpg', 34, '<p>Do lado de fora, amigos se revezavam para saber detalhes da viagem e para conhecer o carro mais popular do Brasil, a Kombi, &nbsp;transformada em casa e preparado para uma volta ao mundo em 1825 dias.&nbsp;</p>\n<p>&nbsp;</p>', '-1'),
(40, '984300_532805700109514_1143005899_n.jpg', 34, '<p>Entre abra&ccedil;os deixamos S&atilde;o Paulo.</p>', '-1'),
(41, '579457_532808490109235_170429478_n.jpg', 34, '<p>Como ser&aacute; o desenrolar desta aventura?</p>\n<p>Leia os pr&oacute;ximos epis&oacute;dios de nosso blog</p>', '-1'),
(42, 'img_4475.jpg', 35, '<p>Nossa primeira pr&oacute;xima parada: Goioer&ecirc; - PR</p>\n<p>Cidade onde vivem os familiares da In&ecirc;s. A Kombi ser&aacute; documentada aqui.&nbsp;</p>\n<p>Per&iacute;odo estimado de perman&ecirc;ncia: 15 dias</p>', '-1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home_chamadas`
--

DROP TABLE IF EXISTS `home_chamadas`;
CREATE TABLE IF NOT EXISTS `home_chamadas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `destino` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `home_chamadas`
--

INSERT INTO `home_chamadas` (`id`, `titulo`, `destino`, `ordem`) VALUES
(1, 'Estamos na estrada', 'http://voudekombi.com/diario/ler/de_sao_paulo_para_goioere', 0),
(3, 'Confira nossos apoiadores!', 'http://voudekombi.com/apoiadores', 2),
(4, 'Nosso adeus a São Paulo', 'http://voudekombi.com/diario/ler/nosso_adeus_a_sao_paulo_1', -1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `home_frases`
--

DROP TABLE IF EXISTS `home_frases`;
CREATE TABLE IF NOT EXISTS `home_frases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(255) DEFAULT NULL,
  `caixa` int(11) NOT NULL DEFAULT '2' COMMENT 'caixas:\n1 - Azul à esquerda\n2 - Preta à direita',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Extraindo dados da tabela `home_frases`
--

INSERT INTO `home_frases` (`id`, `texto`, `caixa`) VALUES
(1, 'AS COISAS MAIS INTERESSANTES ESTÃO FORA DA ROTA PRINCIPAL', 2),
(2, 'VIVER NA KOMBI É: Ter um encontro marcado em cada cidade!', 1),
(7, 'VIVER NA KOMBI É: Procurar todos os dias um lugar onde parar.', 1),
(8, 'VIVER NA KOMBI É: Dormir apertadinho', 1),
(9, 'VIVER NA KOMBI É: Fazer amigos ao longo do caminho.', 1),
(10, 'VIVER NA KOMBI É: Ter pouca roupa.', 1),
(11, 'VIVER NA KOMBI É: Caçar loucamente um sinal Wi-Fi', 1),
(12, 'O que define um viajante é a liberdade de mudar de rota.', 2),
(13, 'A liberdade é o nosso maior bem.', 2),
(14, 'Cada momento é único e não se repete.', 2),
(15, 'Há muito para aprender com um velho ancião.', 2),
(16, 'É preciso se aproximar para ver as belezas escondidas na simplicidade, pobreza, no aparentemente feio.', 2),
(17, 'A viagem precisa de longas pausas para o descanso.', 2),
(18, 'Somos caçadores de riquezas imateriais. ', 2),
(19, 'Documentar é uma forma de preservar.', 2),
(20, 'É preciso bem pouco para ser feliz.', 2),
(21, 'A beleza está no modo como olhamos e não no objeto em si.', 2),
(22, 'Mirar e admirar sempre.', 2),
(23, 'Aceitar as surpresas que transtornam nossos planos e modificam nossos sonhos.', 2),
(24, 'Não há acaso.', 2),
(25, 'Rir muito, se emocionar sempre, chorar quando for preciso.', 2),
(26, 'Errar é só um jeito de aprender.', 2),
(27, 'Viajar é tecer uma trama de inter-relações.', 2),
(28, 'Sempre que for possível plante um árvore.', 2),
(29, 'Consumir menos para que o planeta seja um lugar melhor de viver.', 2),
(30, 'Se há algo porque lutar que seja por um mundo mais justo, mais humano, mais igualitário.', 2),
(31, 'Desvelar belezas escondidas no cotidiano de quem é "quase invisível".', 2),
(32, 'Não se preocupe tudo vai dar certo.', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paises`
--

DROP TABLE IF EXISTS `paises`;
CREATE TABLE IF NOT EXISTS `paises` (
  `iso` char(2) NOT NULL,
  `iso3` char(3) NOT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`iso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `paises`
--

INSERT INTO `paises` (`iso`, `iso3`, `numcode`, `nome`) VALUES
('AD', 'AND', 20, 'Andorra'),
('AE', 'ARE', 784, 'Emiratos Árabes Unidos'),
('AF', 'AFG', 4, 'Afeganistão'),
('AG', 'ATG', 28, 'Antigua e Barbuda'),
('AI', 'AIA', 660, 'Anguilla'),
('AL', 'ALB', 8, 'Albânia'),
('AM', 'ARM', 51, 'Arménia'),
('AN', 'ANT', 530, 'Antilhas Holandesas'),
('AO', 'AGO', 24, 'Angola'),
('AQ', 'ATA', 10, 'Antárctida'),
('AR', 'ARG', 32, 'Argentina'),
('AS', 'ASM', 16, 'Samoa Americana'),
('AT', 'AUT', 40, 'Áustria'),
('AU', 'AUS', 36, 'Austrália'),
('AW', 'ABW', 533, 'Aruba'),
('AX', 'ALA', 248, 'Åland, Ilhas'),
('AZ', 'AZE', 31, 'Azerbeijão'),
('BA', 'BIH', 70, 'Bósnia-Herzegovina'),
('BB', 'BRB', 52, 'Barbados'),
('BD', 'BGD', 50, 'Bangladesh'),
('BE', 'BEL', 56, 'Bélgica'),
('BF', 'BFA', 854, 'Burkina Faso'),
('BG', 'BGR', 100, 'Bulgária'),
('BH', 'BHR', 48, 'Bahrain'),
('BI', 'BDI', 108, 'Burundi'),
('BJ', 'BEN', 204, 'Benin'),
('BM', 'BMU', 60, 'Bermuda'),
('BN', 'BRN', 96, 'Brunei'),
('BO', 'BOL', 68, 'Bolívia'),
('BR', 'BRA', 76, 'Brasil'),
('BS', 'BHS', 44, 'Bahamas'),
('BT', 'BTN', 64, 'Butão'),
('BV', 'BVT', 74, 'Bouvet, Ilha'),
('BW', 'BWA', 72, 'Botswana'),
('BY', 'BLR', 112, 'Bielo-Rússia'),
('BZ', 'BLZ', 84, 'Belize'),
('CA', 'CAN', 124, 'Canadá'),
('CC', 'CCK', 166, 'Cocos, Ilhas'),
('CD', 'COD', 180, 'Congo, República Democrática do (antigo Zaire)'),
('CF', 'CAF', 140, 'Centro-africana, República'),
('CG', 'COG', 178, 'Congo, República do'),
('CH', 'CHE', 756, 'Suíça'),
('CI', 'CIV', 384, 'Costa do Marfim'),
('CK', 'COK', 184, 'Cook, Ilhas'),
('CL', 'CHL', 152, 'Chile'),
('CM', 'CMR', 120, 'Camarões'),
('CN', 'CHN', 156, 'China'),
('CO', 'COL', 170, 'Colômbia'),
('CR', 'CRI', 188, 'Costa Rica'),
('CU', 'CUB', 192, 'Cuba'),
('CV', 'CPV', 132, 'Cabo Verde'),
('CX', 'CXR', 162, 'Christmas, Ilha'),
('CY', 'CYP', 196, 'Chipre'),
('CZ', 'CZE', 203, 'Checa, República'),
('DE', 'DEU', 276, 'Alemanha'),
('DJ', 'DJI', 262, 'Djibouti'),
('DK', 'DNK', 208, 'Dinamarca'),
('DM', 'DMA', 212, 'Dominica'),
('DO', 'DOM', 214, 'Dominicana, República'),
('DZ', 'DZA', 12, 'Argélia'),
('EC', 'ECU', 218, 'Equador'),
('EE', 'EST', 233, 'Estónia'),
('EG', 'EGY', 818, 'Egipto'),
('EH', 'ESH', 732, 'Saara Ocidental'),
('ER', 'ERI', 232, 'Eritreia'),
('ES', 'ESP', 724, 'Espanha'),
('ET', 'ETH', 231, 'Etiópia'),
('FI', 'FIN', 246, 'Finlândia'),
('FJ', 'FJI', 242, 'Fiji'),
('FK', 'FLK', 238, 'Malvinas, Ilhas (Falkland)'),
('FM', 'FSM', 583, 'Micronésia, Estados Federados da'),
('FO', 'FRO', 234, 'Faroe, Ilhas'),
('FR', 'FRA', 250, 'França'),
('GA', 'GAB', 266, 'Gabão'),
('GB', 'GBR', 826, 'Reino Unido da Grã-Bretanha e Irlanda do Norte'),
('GD', 'GRD', 308, 'Grenada'),
('GE', 'GEO', 268, 'Geórgia'),
('GF', 'GUF', 254, 'Guiana Francesa'),
('GG', 'GGY', 831, 'Guernsey'),
('GH', 'GHA', 288, 'Gana'),
('GI', 'GIB', 292, 'Gibraltar'),
('GL', 'GRL', 304, 'Gronelândia'),
('GM', 'GMB', 270, 'Gâmbia'),
('GN', 'GIN', 324, 'Guiné-Conacri'),
('GP', 'GLP', 312, 'Guadeloupe'),
('GQ', 'GNQ', 226, 'Guiné Equatorial'),
('GR', 'GRC', 300, 'Grécia'),
('GS', 'SGS', 239, 'Geórgia do Sul e Sandwich do Sul, Ilhas'),
('GT', 'GTM', 320, 'Guatemala'),
('GU', 'GUM', 316, 'Guam'),
('GW', 'GNB', 624, 'Guiné-Bissau'),
('GY', 'GUY', 328, 'Guiana'),
('HK', 'HKG', 344, 'Hong Kong'),
('HM', 'HMD', 334, 'Heard e Ilhas McDonald, Ilha'),
('HN', 'HND', 340, 'Honduras'),
('HR', 'HRV', 191, 'Croácia'),
('HT', 'HTI', 332, 'Haiti'),
('HU', 'HUN', 348, 'Hungria'),
('ID', 'IDN', 360, 'Indonésia'),
('IE', 'IRL', 372, 'Irlanda'),
('IL', 'ISR', 376, 'Israel'),
('IM', 'IMN', 833, 'Man, Ilha de'),
('IN', 'IND', 356, 'Índia'),
('IO', 'IOT', 86, 'Território Britânico do Oceano Índico'),
('IQ', 'IRQ', 368, 'Iraque'),
('IR', 'IRN', 364, 'Irão'),
('IS', 'ISL', 352, 'Islândia'),
('IT', 'ITA', 380, 'Itália'),
('JE', 'JEY', 832, 'Jersey'),
('JM', 'JAM', 388, 'Jamaica'),
('JO', 'JOR', 400, 'Jordânia'),
('JP', 'JPN', 392, 'Japão'),
('KE', 'KEN', 404, 'Quénia'),
('KG', 'KGZ', 417, 'Quirguistão'),
('KH', 'KHM', 116, 'Cambodja'),
('KI', 'KIR', 296, 'Kiribati'),
('KM', 'COM', 174, 'Comores'),
('KN', 'KNA', 659, 'São Cristóvão e Névis (Saint Kitts e Nevis)'),
('KP', 'PRK', 408, 'Coreia, República Democrática da (Coreia do Norte)'),
('KR', 'KOR', 410, 'Coreia do Sul'),
('KW', 'KWT', 414, 'Kuwait'),
('KY', 'CYM', 136, 'Cayman, Ilhas'),
('KZ', 'KAZ', 398, 'Cazaquistão'),
('LA', 'LAO', 418, 'Laos'),
('LB', 'LBN', 422, 'Líbano'),
('LC', 'LCA', 662, 'Santa Lúcia'),
('LI', 'LIE', 438, 'Liechtenstein'),
('LK', 'LKA', 144, 'Sri Lanka'),
('LR', 'LBR', 430, 'Libéria'),
('LS', 'LSO', 426, 'Lesoto'),
('LT', 'LTU', 440, 'Lituânia'),
('LU', 'LUX', 442, 'Luxemburgo'),
('LV', 'LVA', 428, 'Letónia'),
('LY', 'LBY', 434, 'Líbia'),
('MA', 'MAR', 504, 'Marrocos'),
('MC', 'MCO', 492, 'Mónaco'),
('MD', 'MDA', 498, 'Moldávia'),
('ME', 'MNE', 499, 'Montenegro'),
('MG', 'MDG', 450, 'Madagáscar'),
('MH', 'MHL', 584, 'Marshall, Ilhas'),
('MK', 'MKD', 807, 'Macedónia, República da'),
('ML', 'MLI', 466, 'Mali'),
('MM', 'MMR', 104, 'Myanmar (antiga Birmânia)'),
('MN', 'MNG', 496, 'Mongólia'),
('MO', 'MAC', 446, 'Macau'),
('MP', 'MNP', 580, 'Marianas Setentrionais'),
('MQ', 'MTQ', 474, 'Martinica'),
('MR', 'MRT', 478, 'Mauritânia'),
('MS', 'MSR', 500, 'Montserrat'),
('MT', 'MLT', 470, 'Malta'),
('MU', 'MUS', 480, 'Maurícia'),
('MV', 'MDV', 462, 'Maldivas'),
('MW', 'MWI', 454, 'Malawi'),
('MX', 'MEX', 484, 'México'),
('MY', 'MYS', 458, 'Malásia'),
('MZ', 'MOZ', 508, 'Moçambique'),
('NA', 'NAM', 516, 'Namíbia'),
('NC', 'NCL', 540, 'Nova Caledónia'),
('NE', 'NER', 562, 'Níger'),
('NF', 'NFK', 574, 'Norfolk, Ilha'),
('NG', 'NGA', 566, 'Nigéria'),
('NI', 'NIC', 558, 'Nicarágua'),
('NL', 'NLD', 528, 'Países Baixos (Holanda)'),
('NO', 'NOR', 578, 'Noruega'),
('NP', 'NPL', 524, 'Nepal'),
('NR', 'NRU', 520, 'Nauru'),
('NU', 'NIU', 570, 'Niue'),
('NZ', 'NZL', 554, 'Nova Zelândia (Aotearoa)'),
('OM', 'OMN', 512, 'Oman'),
('PA', 'PAN', 591, 'Panamá'),
('PE', 'PER', 604, 'Peru'),
('PF', 'PYF', 258, 'Polinésia Francesa'),
('PG', 'PNG', 598, 'Papua-Nova Guiné'),
('PH', 'PHL', 608, 'Filipinas'),
('PK', 'PAK', 586, 'Paquistão'),
('PL', 'POL', 616, 'Polónia'),
('PM', 'SPM', 666, 'Saint Pierre et Miquelon'),
('PN', 'PCN', 612, 'Pitcairn'),
('PR', 'PRI', 630, 'Porto Rico'),
('PS', 'PSE', 275, 'Palestina'),
('PT', 'PRT', 620, 'Portugal'),
('PW', 'PLW', 585, 'Palau'),
('PY', 'PRY', 600, 'Paraguai'),
('QA', 'QAT', 634, 'Qatar'),
('RE', 'REU', 638, 'Reunião'),
('RO', 'ROU', 642, 'Roménia'),
('RS', 'SRB', 688, 'Sérvia'),
('RU', 'RUS', 643, 'Rússia'),
('RW', 'RWA', 646, 'Ruanda'),
('SA', 'SAU', 682, 'Arábia Saudita'),
('SB', 'SLB', 90, 'Salomão, Ilhas'),
('SC', 'SYC', 690, 'Seychelles'),
('SD', 'SDN', 736, 'Sudão'),
('SE', 'SWE', 752, 'Suécia'),
('SG', 'SGP', 702, 'Singapura'),
('SH', 'SHN', 654, 'Santa Helena'),
('SI', 'SVN', 705, 'Eslovénia'),
('SJ', 'SJM', 744, 'Svalbard e Jan Mayen'),
('SK', 'SVK', 703, 'Eslováquia'),
('SL', 'SLE', 694, 'Serra Leoa'),
('SM', 'SMR', 674, 'San Marino'),
('SN', 'SEN', 686, 'Senegal'),
('SO', 'SOM', 706, 'Somália'),
('SR', 'SUR', 740, 'Suriname'),
('ST', 'STP', 678, 'São Tomé e Príncipe'),
('SV', 'SLV', 222, 'El Salvador'),
('SY', 'SYR', 760, 'Síria'),
('SZ', 'SWZ', 748, 'Suazilândia'),
('TC', 'TCA', 796, 'Turks e Caicos'),
('TD', 'TCD', 148, 'Chade'),
('TF', 'ATF', 260, 'Terras Austrais e Antárticas Francesas (TAAF)'),
('TG', 'TGO', 768, 'Togo'),
('TH', 'THA', 764, 'Tailândia'),
('TJ', 'TJK', 762, 'Tajiquistão'),
('TK', 'TKL', 772, 'Toquelau'),
('TL', 'TLS', 626, 'Timor-Leste'),
('TM', 'TKM', 795, 'Turquemenistão'),
('TN', 'TUN', 788, 'Tunísia'),
('TO', 'TON', 776, 'Tonga'),
('TR', 'TUR', 792, 'Turquia'),
('TT', 'TTO', 780, 'Trindade e Tobago'),
('TV', 'TUV', 798, 'Tuvalu'),
('TW', 'TWN', 158, 'Taiwan'),
('TZ', 'TZA', 834, 'Tanzânia'),
('UA', 'UKR', 804, 'Ucrânia'),
('UG', 'UGA', 800, 'Uganda'),
('UM', 'UMI', 581, 'Menores Distantes dos Estados Unidos, Ilhas'),
('US', 'USA', 840, 'Estados Unidos da América'),
('UY', 'URY', 858, 'Uruguai'),
('UZ', 'UZB', 860, 'Usbequistão'),
('VA', 'VAT', 336, 'Vaticano'),
('VC', 'VCT', 670, 'São Vicente e Granadinas'),
('VE', 'VEN', 862, 'Venezuela'),
('VG', 'VGB', 92, 'Virgens Britânicas, Ilhas'),
('VI', 'VIR', 850, 'Virgens Americanas, Ilhas'),
('VN', 'VNM', 704, 'Vietname'),
('VU', 'VUT', 548, 'Vanuatu'),
('WF', 'WLF', 876, 'Wallis e Futuna'),
('WS', 'WSM', 882, 'Samoa (Samoa Ocidental)'),
('YE', 'YEM', 887, 'Iémen'),
('YT', 'MYT', 175, 'Mayotte'),
('ZA', 'ZAF', 710, 'África do Sul'),
('ZM', 'ZMB', 894, 'Zâmbia'),
('ZW', 'ZWE', 716, 'Zimbabwe');

-- --------------------------------------------------------

--
-- Estrutura da tabela `roteiro`
--

DROP TABLE IF EXISTS `roteiro`;
CREATE TABLE IF NOT EXISTS `roteiro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(2) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `chegamos` int(11) NOT NULL DEFAULT '0',
  `data_chegada` datetime DEFAULT NULL,
  `descritivo` text,
  `imagem` varchar(140) DEFAULT NULL,
  `slug_blog` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(140) NOT NULL,
  `email` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `NomeUsuario` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `password`, `email`) VALUES
(13, 'trupe', 'd32c9694c72f1799ec545c82c8309c02', 'contato@trupe.net'),
(14, 'alice', '6384e2b2184bcbf58eccf10ca7a6563c', 'contato@voudekombi.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
