$(document).ready( function(){

    $('.voltar').click( function(){ window.history.back(); });

    $('#datepicker').datepicker();

    $('#monthpicker').monthpicker();

    if($('.mostrarerro').length || $('.mostrarsucesso').length){
        console.log('erro')
        $('.mensagem').show('normal', function(){
            $(this).delay(4000).slideUp('normal');
        })
    }

    $('.delete').click( function(){
        if(!confirm('Deseja Excluir o Registro ?'))
            return false;
    });

    $('input[type="checkbox"]').change( function(){
        if($(this).is(':checked')){
            $(this).parent().css('font-weight', 'bold');
        }else{
            $(this).parent().css('font-weight', 'normal');
        }
    });

    $('input[type="checkbox"]:checked').each( function(){
        $(this).parent().css('font-weight', 'bold');
    })   

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "comimagem",
        theme : "advanced",
        theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink,separator,image",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_blockformats : "h1, h2, p",
        theme_advanced_toolbar_location : "top",
        plugins: "paste,advimage",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },        
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "semimagem",
        theme : "advanced",
        theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_blockformats : "h1, h2, p",
        theme_advanced_toolbar_location : "top",
        plugins: "paste",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },        
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "basico",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        plugins: "paste",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        width:660,
        height:150
    });    
});