var redimensionaChamadas = function(){	
	if($('.chamadas .chamada').length){
		$('.chamadas .chamada').each( function(){
			$(this).css('width', '100%');
			$(this).css('width', $(this).find('span').css('width'));
		});
	}
};

var ajustaPosicaoCorpo = function(){
	var corpo = $('.corpo');
	if(!Modernizr.mq("only screen and (min-width: 481px) and (max-width: 1024px) and (orientation:landscape)") && !Modernizr.mq("only screen and (min-width: 481px) and (max-width: 1024px) and (orientation:portrait)") && !Modernizr.mq("only screen and (max-device-width:480px)")){	
		if(corpo.offset().top > 0){
			corpo.css({
				position : 'absolute',
				top: 0,
				left : $('#header-spacer').css('width')
			});		
		}else{
			corpo.css({
				position : 'relative',
				top: 0,
				left : 0
			});
		}
	}
};

var ajustaAlturaMenu = function(){
	$('#header-spacer').css('height', $('header').css('height'));		
};

var cycleSlides = function(){

	if ($('#slides-home').length) {
		if($('#slides-home').hasClass('galeria')){
			$('#slides-home').cycle('stop').cycle({
				timeout : 0,
				containerResize : 0,
				slideResize : 0,
				fx : 'scrollHorz',
				prev : $('.galeria-prev'),
				next : $('.galeria-next'),
			});
		}else{
		
			$('#slides-home').cycle('stop').cycle({
				timeout : 3699,
				containerResize : 0,
				slideResize : 0
			});
		}

	};

}

var init = function(){
	redimensionaChamadas();
	ajustaPosicaoCorpo();
	ajustaAlturaMenu();
	cycleSlides();
};

$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);

	$('.frases-preto').cycle({
		timeout : 7250,
		containerResize : 0,
		slideResize : 0,
		after: function(currSlideElement, nextSlideElement, options, forwardFlag){
			if(!Modernizr.mq("only screen and (min-width: 481px) and (max-width: 1024px) and (orientation:landscape)") && !Modernizr.mq("only screen and (min-width: 481px) and (max-width: 880px) and (orientation:portrait)")){
				$(currSlideElement).css({
					right : 0,
					left: 'auto'
				});
				$(nextSlideElement).css({
					right : 0,
					left: 'auto'
				});
			}else{
				$(currSlideElement).css({
					right : 'auto',
					left: 0
				});
				$(nextSlideElement).css({
					right : 'auto',
					left: 0
				});
			}
		},
		before: function(currSlideElement, nextSlideElement, options, forwardFlag){
			if(!Modernizr.mq("only screen and (min-width: 481px) and (max-width: 1024px) and (orientation:landscape)") && !Modernizr.mq("only screen and (min-width: 481px) and (max-width: 880px) and (orientation:portrait)")){
				$(currSlideElement).css({
					right : 0,
					left: 'auto'
				});
				$(nextSlideElement).css({
					right : 0,
					left: 'auto'
				});
			}else{
				$(currSlideElement).css({
					right : 'auto',
					left: 0
				});
				$(nextSlideElement).css({
					right : 'auto',
					left: 0
				});
			}
		}
	});

	$('.frases-azul').cycle({
		timeout : 6479,
		containerResize : 0,
		slideResize : 0
	});

	$(window).on('load resize', function(){
		init();
	});

	$('.fit').fitText(1.2);

	$('#form-comentario').submit( function(){
		if ($("input[name=nome]").val() == '' || $("input[name=nome]").val() == $("input[name=nome]").attr('placeholder')) {
			alert('Informe seu nome!');
			return false;
		};
		if ($("input[name=email]").val() == '' || $("input[name=email]").val() == $("input[name=email]").attr('placeholder')) {
			alert('Informe seu e-mail!');
			return false;
		};
		if ($("textarea[name=mensagem]").val() == '' || $("textarea[name=mensagem]").val() == $("textarea[name=mensagem]").attr('placeholder')) {
			alert('Informe sua mensagem!');
			return false;
		};
	});

	$('.abre-categorias').click( function(e){
		e.preventDefault();
		if($('#cat_recentes li.hid').length){
			$('#cat_recentes li.hid').removeClass('hid');
			$(this).html("ESCONDER CATEGORIAS &raquo;");
		}else{
			$('#cat_recentes li:gt(2)').addClass('hid');
			$(this).html("VER TODAS AS CATEGORIAS &raquo;");			
		}
	});

	$('.passamos ul li a').click( function(e){
		e.preventDefault();
		$('.passamos ul li a.ativo').removeClass('ativo');
		$(this).addClass('ativo');
		var buscar = $(this).attr('data-pais');
		$('.passamos .thumbs a').each( function(){
			if($(this).attr('data-pais') == buscar){
				$(this).removeClass('hidden');
			}else{
				$(this).addClass('hidden');
			}
		});
	});

	if(Modernizr.mq("only screen and (min-width: 481px) and (max-width: 1024px) and (orientation:landscape)") || Modernizr.mq("only screen and (min-width: 481px) and (max-width: 880px) and (orientation:portrait)")){
		window.onscroll = function() {
			$("header").css("top", $("body").scrollTop());
		};
	}

});