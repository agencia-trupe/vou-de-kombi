var ajusta_largura = function(){
	var ultimo_elemento = $('.principal .bgazul *:last');
	var altura_ultimo_elemento = parseInt(ultimo_elemento.css('height'))
	var offset_ultimo_elemento = parseInt(ultimo_elemento.offset().top);
	var offset_total = altura_ultimo_elemento + offset_ultimo_elemento;
	var altura_bg_azul = parseInt($('.principal .bgazul').css('height'));
	var cont = 0;

	if(offset_total > altura_bg_azul){
		$('.principal .bgazul p').addClass('notransition');
		while(offset_total > altura_bg_azul){				
			console.log('ajuste n : ' + cont);
			$('.principal .bgazul p').css('width', parseInt($('.principal .bgazul').css('width')) + 30);
			altura_ultimo_elemento = parseInt(ultimo_elemento.css('height'))
			offset_ultimo_elemento = parseInt(ultimo_elemento.offset().top);
			offset_total = altura_ultimo_elemento + offset_ultimo_elemento;			
			cont++;
		}
		$('.principal .bgazul p.notransition').removeClass('notransition');
	}	
}

$('document').ready( function(){

	var largura_tela = parseInt($('body').css('width'));

	$('.main .content .principal').css('width', largura_tela - 415);

	ajusta_largura();

	$('header nav ul li a').click( function(e){
		e.preventDefault();
		var alvo = $('.principal .bgazul');

		if(!$(this).hasClass('ativo')){

			var destino = $(this).attr('href');

			$('nav .ativo').removeClass('ativo');
			$(this).addClass('ativo');

			$.get(BASE+destino+" #load", function(data){
				var resultado = $(data).find('.ajax-load');
				$('.main').attr('class', 'main main-'+destino);
				window.location.hash = destino;
				alvo.fadeOut('normal', function(){
					alvo.html(resultado).fadeIn('normal', function(){
						ajusta_largura();	
					});
				});
			});
		}
	});

	newHash = window.location.hash.substring(1);
	if(newHash)
		$('#mn-'+newHash).trigger('click');

});